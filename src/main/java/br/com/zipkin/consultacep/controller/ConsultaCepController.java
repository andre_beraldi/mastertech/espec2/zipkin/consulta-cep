package br.com.zipkin.consultacep.controller;

import br.com.zipkin.consultacep.client.ViaCepDTO;
import br.com.zipkin.consultacep.service.ConsultaCepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsultaCepController {

    @Autowired
    private ConsultaCepService consultaCepService;

    @GetMapping("/consulta-cep/{cep}")
    public ViaCepDTO consultarCep(@PathVariable String cep){
        return consultaCepService.consultarCep(cep);
    }
}
