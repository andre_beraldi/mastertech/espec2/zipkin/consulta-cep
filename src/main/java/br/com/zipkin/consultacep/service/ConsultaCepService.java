package br.com.zipkin.consultacep.service;


import br.com.zipkin.consultacep.client.ViaCepClient;
import br.com.zipkin.consultacep.client.ViaCepDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class ConsultaCepService {
    @Autowired
    private ViaCepClient viaCepClient;

    @NewSpan(name = "consultar-cep-service")
    public ViaCepDTO consultarCep(@SpanTag("cep") String cep) {
        return viaCepClient.ConsultarCep(cep);
    }

}
