package br.com.zipkin.consultacep.client;

public class ViaCepDTO {
    private String logradouro;

    public ViaCepDTO() {
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
}
