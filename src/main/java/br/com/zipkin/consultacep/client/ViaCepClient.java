package br.com.zipkin.consultacep.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "consultaCep", url = "https://viacep.com.br/ws/")
public interface ViaCepClient {

    @GetMapping("/{cep}/json")
    ViaCepDTO ConsultarCep(@PathVariable String cep);

}
